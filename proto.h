// This is free and unencumbered software released into the public domain.
// For more information, see LICENSE.

#ifndef PROTO_H
#define PROTO_H
#include <stdint.h>

#define VER "V0"
#define __pack __attribute__((packed))

struct __pack PkStr {
    uint16_t len;
    char str[];
};

struct __pack Packet {
    uint32_t size;
    uint8_t type;
    uint16_t tag;
    char data[];
};

struct __pack PkTVersion {
    uint32_t msize;
    uint16_t vlen;
    char vstr[];
};

struct __pack PkRVersion {
    uint32_t msize;
    uint16_t vlen;
    char vstr[];
};

struct __pack PkRError {
    uint32_t type;
};

struct __pack PkTLogin {
    struct PkStr *usr;
    struct PkStr *pwd;
};

struct __pack PkRInt {
    int32_t res;
    int32_t err;
};

struct __pack PkTOpen {
    int32_t flags;
    int32_t mode;
    uint16_t plen;
    char pstr[];
};

struct __pack PkTChdir {
    uint16_t plen;
    char pstr[];
};

struct __pack PkTGetdents64 {
    int32_t fd;
    uint32_t count;
};

struct __pack PkRGetdents64 {
    int32_t res;
    int32_t err;
    char dirp[];
};

struct __pack PkTClose {
    int32_t fd;
};

enum ERR {
    UNHANDLED,
    MISSING_VEXCHANGE,
    BUFFER_TOO_SMALL,
    LOGIN_NO_USER,
    LOGIN_NO_HOME,
    VEXCHANGE_DENIED,
    LOGIN_OK,
    MALFORMED_PACKET,
    LOGIN_DENIED,
    LOCAL_RESOURCE,

    MAX_ERROR,
};

enum TYPE {
    TError,
    RError,
    TVersion,
    RVersion,
    TLogin,
    RLogin,

    TOpen,
    TClose,
    TChdir,
    RInt,
    TGetdents64,
    RGetdents64,

    MAX_TYPE,
};

enum {
    TRANSMIT,
    RECEIVE,
};


#endif //PROTO_H
