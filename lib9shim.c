// This is free and unencumbered software released into the public domain.
// For more information, see LICENSE.

#define _GNU_SOURCE
#include <stdarg.h>
#include <fcntl.h>
#include <pthread.h>
#include <dlfcn.h>

#include "proto.c"

static int sfd;
static char buf[8096];
static _Atomic(uint16_t) tag = 0;
static pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;
static char *env[] = {
    "LD_PRELOAD=/home/lorenzo/dev/lib9/lib9.so",
    NULL,
};

struct dirstream {
	off_t tell;
	int fd;
	int buf_pos;
	int buf_end;
	volatile int lock[1];
	char buf[2048];
};

struct dirent {
    ino_t d_ino;
    off_t d_off;
    unsigned short d_reclen;
    unsigned char d_type;
    char d_name[];
};

static int (*fnopen)(const char *, int, ...);
static int (*fnclose)(int);
static int (*fnchdir)(const char *);
static struct dirstream *(*fnopendir)(const char *);
static struct dirent *(*fnreaddir)(struct dirstream *);
static int (*fnclosedir)(struct dirstream *);
static int (*fnexecve)(const char *, char *const[], char *const[]);
static int (*fnexecvpe)(const char *, char *const[], char *const[]);
static int (*fngetdents64)(unsigned int, char *, unsigned int);

void __attribute__((constructor))
initsession(void) {
    if ((sfd = clisession()) == -1) {
        return;
    }
    if (clipreamble(sfd, "anon", buf, sizeof(buf)) == -1) {
        shutdown(sfd, SHUT_RDWR);
        return;
    }
    fnopen = dlsym(RTLD_NEXT, "open");
    fnclose = dlsym(RTLD_NEXT, "close");
    fnchdir = dlsym(RTLD_NEXT, "chdir");
    fnopendir = dlsym(RTLD_NEXT, "opendir");
    fnreaddir = dlsym(RTLD_NEXT, "readdir");
    fnclosedir = dlsym(RTLD_NEXT, "closedir");
    fnexecve = dlsym(RTLD_NEXT, "execve");
    fnexecvpe = dlsym(RTLD_NEXT, "execvpe");
    fngetdents64 = dlsym(RTLD_NEXT, "getdents64");
}

int
open(const char *fname, int flags, ...) {
    struct Packet *tp = mkopen(buf, sizeof(buf), tag++, 0, 0, fname), *rp = tp;
    struct PkRInt *ri = (void *)rp->data;
    struct PkRError *re = (void *)rp->data;
    mode_t mode = 0;
    int err = -1;

    if ((flags & O_CREAT) || (flags & O_TMPFILE) == O_TMPFILE) {
        va_list ap;
        va_start(ap, flags);
        mode = va_arg(ap, mode_t);
        va_end(ap);
    }
    tp = mkopen(buf, sizeof(buf), tag++, 0, 0, fname);
    if (pthread_mutex_lock(&m) != 0) {
        return err;
    }
    if (send_con(sfd, tp) == -1) {
        goto FINOPEN;
    }
    if ((rp = recv_con(sfd, buf, sizeof(buf))) == NULL
        || pck(tp->tag, rp, sizeof(buf)) == -1
    ) {
        goto FINOPEN;
    }
    ri = (void *)rp->data;
    switch (rp->type) {
        case RInt:
            err = ri->res;
            errno = ri->err;
            break;
        case RError:
            if (re->type == LOCAL_RESOURCE) {
                err = fnopen(fname, flags, mode);
            }
            break;
    }
FINOPEN:
    pthread_mutex_unlock(&m);
    return err;
}

int
unlink(const char *path) {

}

int
chdir(const char *fname) {
    struct Packet *tp = mkchdir(buf, sizeof(buf), tag++, fname), *rp = tp;
    struct PkRInt *ri = (void *)rp->data;
    int err = -1;

    if (fnchdir(fname) != 0) {
        return err;
    }
    if (pthread_mutex_lock(&m) != 0) {
        return err;
    }
    if (send_con(sfd, tp) == -1) {
        goto FINCHDIR;
    }
    if ((rp = recv_con(sfd, buf, sizeof(buf))) == NULL
        || pck(tp->tag, rp, sizeof(buf)) == -1
    ) {
        goto FINCHDIR;
    }
    switch (rp->type) {
        case RInt:
            err = ri->res;
            errno = ri->err;
            break;
    }
FINCHDIR:
    pthread_mutex_unlock(&m);
    return err;
}

int
close(int fd) {
    struct Packet *tp = mktclose(buf, sizeof(buf), tag++, fd);
    struct Packet *rp = tp;
    struct PkRInt *ri = (void *)rp->data;
    struct PkRError *re = (void *)rp->data;
    int err = -1;

    if (pthread_mutex_lock(&m) != 0) {
        return err;
    }
    if (send_con(sfd, tp) == -1) {
        goto FINCLOSE;
    }
    if ((rp = recv_con(sfd, buf, sizeof(buf))) == NULL
        || pck(tp->tag, rp, sizeof(buf)) == -1
    ) {
        goto FINCLOSE;
    }
    switch (rp->type) {
        case RInt:
            err = ri->res;
            errno = ri->err;
            break;
        case RError:
            if (re->type == LOCAL_RESOURCE) {
                err = fnclose(fd);
            }
            break;
    }
FINCLOSE:
    pthread_mutex_unlock(&m);
    return err;
}



int
getdents64(unsigned int fd, char *dirp, unsigned int count) {
    struct Packet *tp = mktgetdents64(buf, sizeof(buf), tag++, fd, count);
    struct Packet *rp = tp;
    struct PkRGetdents64 *rg = (void *)rp->data;
    struct PkRError *re = (void *)rp->data;
    int err = -1;

    if (pthread_mutex_lock(&m) != 0) {
        return err;
    }
    if (send_con(sfd, tp) == -1) {
        goto FINGETDENTS64;
    }
    if ((rp = recv_con(sfd, buf, sizeof(buf))) == NULL
        || pck(tp->tag, rp, sizeof(buf)) == -1
    ) {
        goto FINGETDENTS64;
    }
    switch (rp->type) {
        case RGetdents64:
            err = rg->res;
            errno = rg->err;
            if (err > 0) {
                memcpy(dirp, rg->dirp, err);
            }
        case RError:
            if (re->type == LOCAL_RESOURCE) {
                err = fngetdents64(fd, dirp, count);
            }
            break;
            break;
    }
FINGETDENTS64:
    pthread_mutex_unlock(&m);
    return err;
}

struct dirstream *
opendir(const char *name) {
    int fd;
    struct dirstream *dir = NULL;

    if ((fd = open(name, O_RDONLY|O_DIRECTORY|O_CLOEXEC)) < 0) {
        return dir;
    }
    if ((dir = calloc(1, sizeof(*dir))) == NULL) {
        close(fd);
        return dir;
    }
    dir->fd = fd;
    return dir;
}

int
closedir(struct dirstream *d) {
    int res = close(d->fd);
    free(d);
    return res;
}

struct dirent *
readdir(struct dirstream *dir) {
    struct dirent *de;

    if (dir->buf_pos >= dir->buf_end) {
        int res = getdents64(dir->fd, dir->buf, sizeof(dir->buf));
        if (res <= 0) {
            return NULL;
        }
        dir->buf_end = res;
        dir->buf_pos = 0;
    }
    de = (void *)(dir->buf + dir->buf_pos);
    dir->buf_pos += de->d_reclen;
    dir->tell = de->d_off;
    return de;
}

int
execl(const char *path, char *arg, ...) {
    char **args = malloc(sizeof(arg));
    size_t size = 1;

    va_list ap;
    va_start(ap, arg);

    if (args == NULL) {
        return -1;
    }
    do {
        char *arg = va_arg(ap, char *);
        if (arg == NULL) {
            break;
        }
        void *tmp = realloc(args, ++size * sizeof(*args));
        if (tmp == NULL) {
            free(args);
            return -1;
        }
        args = tmp;
        args[size - 1] = arg;
    } while (1);
    return fnexecve(path, args, env);
}

int
execlp(const char *path, char *arg, ...) {
    char **args = malloc(sizeof(arg));
    size_t size = 1;

    va_list ap;
    va_start(ap, arg);

    if (args == NULL) {
        return -1;
    }
    do {
        char *arg = va_arg(ap, char *);
        if (arg == NULL) {
            break;
        }
        void *tmp = realloc(args, ++size * sizeof(*args));
        if (tmp == NULL) {
            free(args);
            return -1;
        }
        args = tmp;
        args[size - 1] = arg;
    } while (1);
    return fnexecvpe(path, args, env);
}

int
execle(const char *path, char *arg, ...) {
    char **args = malloc(sizeof(arg));
    size_t size = 1;

    va_list ap;
    va_start(ap, arg);

    if (args == NULL) {
        return -1;
    }
    do {
        char *arg = va_arg(ap, char *);
        if (arg == NULL) {
            break;
        }
        void *tmp = realloc(args, ++size * sizeof(*args));
        if (tmp == NULL) {
            free(args);
            return -1;
        }
        args = tmp;
        args[size - 1] = arg;
    } while (1);
    return fnexecve(path, args, env);
}

int
execv(const char *path, char *const argv[]) {
    return fnexecve(path, argv, env);
}

int
execvp(const char *path, char *const argv[]) {
    return fnexecvpe(path, argv, env);
}

int
execve(const char *path, char *const argv[], char *const envp[]) {
    return fnexecve(path, argv, env);
}

int
execvpe(const char *path, char *const argv[]) {
    return fnexecvpe(path, argv, env);
}


