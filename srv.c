// This is free and unencumbered software released into the public domain.
// For more information, see LICENSE.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <err.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <linux/limits.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <fcntl.h>
#include <syscall.h>

#include "proto.h"
#include "proto.gen.h"

#define PORT "12345"
#define BACKLOG (16)
#define MAX_BUF (65536)
#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))
#define cxsize(a) (sizeof(a) / sizeof(*a))

static _Atomic(size_t) reverse = 0;
static _Atomic(int) mapfd[1024];
static const char *blacklist[] = {
    "/home/",
    "/proc/",
    "/sys/",
    "/dev/",
    "/usr/",
    "/etc/",

    NULL
};

struct dirent {
    ino_t d_ino;
    off_t d_off;
    unsigned short d_reclen;
    unsigned char d_type;
    char d_name[];
};

void *
get_in_addr(struct sockaddr *sa) {
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }
    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

void
rev(char *s) {
    size_t len = strlen(s);

    for (size_t i = 0; i < len/2; i++) {
        char tmp = s[i];
        s[i] = s[len - 1 - i];
        s[len - 1 - i] = tmp;
    }
}

uint32_t
fmemcmp(const char *restrict f, const char *restrict s, uint32_t size) {
    uint32_t i = 0;
    while (i < size) {
        if (f[i] != s[i]) {
            break;
        }
        i++;
    }
    return i;
}

void
run_session(int fd) {
    uint32_t size = 4096, msize, ss;
    void *buf = malloc(size);
    struct Packet *tp, *rp;
    char path[PATH_MAX];
    char *usr = NULL;
    char *tmp = NULL;
    struct stat sb;
    int err;
VEXCHANGE:
    if (buf == NULL) {
        return;
    }
    if ((tp = recv_con(fd, buf, size)) == NULL) {
        return;
    }
    switch (tp->type) {
    default:
        rp = mkerr(buf, size, tp->tag, MISSING_VEXCHANGE);
        if (send_con(fd, rp) == -1) {
            warn("send_conn");
            return;
        }
        goto VEXCHANGE;
    case TVersion:
        {
            struct PkTVersion *tvp = (struct PkTVersion *)tp->data;
            msize = min(MAX_BUF, tvp->msize);
            void *new_buf = realloc(buf, msize);
            if (new_buf == NULL) {
                msize = size;
            } else {
                buf = new_buf;
                tp = rp = (struct Packet *)buf;
            }
        }
        size = msize;
        rp = mkver(buf, size, msize, tp->tag, "V0", RECEIVE);
        if (rp == NULL) {
            rp = mkerr(buf, size, tp->tag, MISSING_VEXCHANGE);
            send_con(fd, rp);
            goto VEXCHANGE;
        }
        if (send_con(fd, rp) == -1) {
            warn("send_conn");
            return;
        }
        break;
    }
    rp = mkerr(buf, size, tp->tag, UNHANDLED);
    while (1) {
        if ((tp = recv_con(fd, buf, size)) == NULL) {
            warn("connection closed, goodbye");
            return;
        }
        if (pck(tp->tag, tp, size) == -1) {
            warnx("malformed packet, closing connection");
            rp = mkerr(buf, size, tp->tag, MALFORMED_PACKET);
            send_con(fd, rp);
            return;
        }
        switch (tp->type) {
        case TVersion:
            rp = mkerr(buf, size, tp->tag, VEXCHANGE_DENIED);
            break;
        case TLogin:
            {
                err = 0;
                struct PkStr *ustr = (struct PkStr *)tp->data;
                struct PkStr *hstr = (struct PkStr *)(
                    tp->data + sizeof(struct PkStr) + ustr->len
                );
                if (usr != NULL || tmp != NULL || hstr->len == 0
                    || ustr->len == 0
                ) {
                    err = -1;
                    break;
                }
                if (ownstr(ustr, &usr) == -1) {
                    err = -1;
                    break;
                }
                if (ownstr(hstr, &tmp) == -1) {
                    free(usr);
                    usr = NULL;
                    err = -1;
                    break;
                }
                if (stat(tmp, &sb) == -1 || (sb.st_mode & S_IFMT) != S_IFDIR) {
                    err = -1;
                    warn("stat");
                    free(usr);
                    free(tmp);
                    usr = tmp = NULL;
                }
                if (realpath(tmp, path) == NULL) {
                    err = -1;
                    break;
                }
                if (err == -1) {
                    rp = mkerr(buf, size, tp->tag, LOGIN_DENIED);
                } else {
                    rp = mkrlogin(buf, size, tp->tag, path);
                }
                warnx("%s@%s", usr, path);
            }
            break;
        case TChdir:
            {
                struct PkStr *pstr = (struct PkStr *)tp->data;

                if (pstr->str[0] == '/') {
                    if (ownstr(pstr, &tmp) == -1) {
                        rp = mkerr(buf, size, tp->tag, BUFFER_TOO_SMALL);
                        break;
                    }
                } else {
                    if ((tmp = realloc(tmp, sizeof(path) + pstr->len + 1))
                        == NULL
                    ) {
                        rp = mkerr(buf, size, tp->tag, BUFFER_TOO_SMALL);
                        break;
                    }
                    size_t len = strlen(path);
                    memcpy(tmp, path, len);
                    tmp[len] = '/';
                    memcpy(tmp + len + 1, pstr->str, pstr->len);
                    tmp[len + pstr->len + 1] = '\0';
                }
                if (realpath(tmp, path) == NULL) {
                    rp = mkrint(buf, size, tp->tag, -1, errno);
                    break;
                }
                if (stat(path, &sb) == -1 || (sb.st_mode & S_IFMT) != S_IFDIR) {
                    rp = mkrint(buf, size, tp->tag, -1, errno);
                    break;
                }
                rp = mkrint(buf, size, tp->tag, 0, errno);
                warnx("chdir %s", path);
            }
            break;
        case TOpen:
            {
                struct PkTOpen *to = (struct PkTOpen *)tp->data;
                struct PkStr *pstr = (struct PkStr *)(
                    tp->data + 2 * sizeof(int32_t)
                );
                for (const char **path = blacklist; *path; path++) {
                    ss = strlen(*path);
                    if (fmemcmp(pstr->str, *path, min(pstr->len, ss)) == ss) {
                        rp = mkerr(buf, size, tp->tag, LOCAL_RESOURCE);
                        goto FINRUN;
                    }
                }
                ss = strlen("reverse");
                if (fmemcmp(pstr->str, "reverse", min(pstr->len, ss)) == ss) {
                    reverse = !reverse;
                    rp = mkrint(buf, size, tp->tag, -1, ENOENT);
                    break;
                }
                if (pstr->str[0] == '/') {
                    if (ownstr(pstr, &tmp) == -1) {
                        rp = mkrint(buf, size, tp->tag, -1, EOVERFLOW);
                        break;
                    }
                } else {
                    if ((tmp = realloc(tmp, sizeof(path) + pstr->len + 1))
                        == NULL
                    ) {
                        rp = mkrint(buf, size, tp->tag, -1, EOVERFLOW);
                        break;
                    }
                    size_t len = strlen(path);
                    memcpy(tmp, path, len);
                    tmp[len] = '/';
                    memcpy(tmp + len + 1, pstr->str, pstr->len);
                    tmp[len + pstr->len + 1] = '\0';
                }
                int fd = open(tmp, to->flags, to->mode);

                if (fd > -1 && (unsigned)fd >= cxsize(mapfd)) {
                    rp = mkrint(buf, size, tp->tag, -1, ENFILE);
                    close(fd);
                    break;
                }
                mapfd[fd] = 1;
                warnx("open %s", tmp);
                rp = mkrint(buf, size, tp->tag, fd, errno);
            }
            break;
        case TClose:
            {
                struct PkTClose *to = (struct PkTClose *)tp->data;
                if (to->fd < 0 || to->fd == fd) {
                    rp = mkrint(buf, size, tp->tag, -1, EBADF);
                    break;
                }
                if ((unsigned)to->fd >= cxsize(mapfd) || mapfd[to->fd] == 0) {
                    rp = mkerr(buf, size, tp->tag, LOCAL_RESOURCE);
                    break;
                }
                mapfd[to->fd] = 0;
                int res = close(to->fd);
                warnx("close %d", to->fd);
                rp = mkrint(buf, size, tp->tag, res, errno);
            }
            break;
        case TGetdents64:
            {
                struct PkTGetdents64 *tg = (struct PkTGetdents64 *)tp->data;
                struct PkRGetdents64 *rg = (struct PkRGetdents64 *)tp->data;

                if ((unsigned)tg->fd >= cxsize(mapfd) || mapfd[tg->fd] == 0) {
                    rp = mkerr(buf, size, tp->tag, LOCAL_RESOURCE);
                    break;
                }
                int res = syscall(SYS_getdents64, tg->fd, rg->dirp, tg->count);

                if (res > 0 && reverse) {
                    for (char *curr = rg->dirp; curr < rg->dirp + res;) {
                        struct dirent *d = (struct dirent *)curr;
                        rev(d->d_name);
                        curr += d->d_reclen;
                    }
                }
                warnx("getdents64 %d", tg->fd);
                rp = mkrgetdents64(buf, size, tp->tag, res, errno, rg->dirp);
            }
            break;
        }
FINRUN:
        send_con(fd, rp);
    }
}

void *
thrun_session(void *arg) {
    int *fd = (int *)arg;
    run_session(*fd);

    return NULL;
}

int
main(void) {
    int sockfd, new_fd;
    struct addrinfo hints = {
        .ai_flags = AI_PASSIVE,
        .ai_family = AF_UNSPEC,
        .ai_socktype = SOCK_STREAM,
    }, *servinfo, *p;
    struct sockaddr_storage addr;
    socklen_t sin_size;
    int yes = 1, res;
    char s[INET6_ADDRSTRLEN];
    pthread_t tid;

    if ((res = getaddrinfo(NULL, PORT, &hints, &servinfo)) != 0) {
        errx(1, "getaddrinfo: %s", gai_strerror(res));
    }
    for(p = servinfo; p != NULL; p = p->ai_next) {
        sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (sockfd == -1) {
            warn("server: socket");
            continue;
        }
        res = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));
        if (res == -1) {
            err(1, "setsockopt");
        }
        if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
            close(sockfd);
            warn("server: bind");
            continue;
        }
        break;
    }
    freeaddrinfo(servinfo);

    if (p == NULL) {
        err(1, "bind");
    }
    if (listen(sockfd, BACKLOG) == -1) {
        err(1, "listen");
    }
    warnx("waiting for connections...");
    while(1) {
        sin_size = sizeof(addr);
        new_fd = accept(sockfd, (struct sockaddr *)&addr, &sin_size);
        if (new_fd == -1) {
            warn("accept");
            continue;
        }
        inet_ntop(addr.ss_family, get_in_addr((struct sockaddr *)&addr), s,
            sizeof(s)
        );
        warnx("got connection from %s", s);
        pthread_create(&tid, NULL, &thrun_session, &new_fd);
        pthread_detach(tid);
    }
    return 0;
}


