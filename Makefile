CFLAGS+=-g -pthread -Wall -Wextra -pedantic
all: proto cli srv shim nil
cli: proto.c call.c
srv: proto.c call.c
proto: proto.c; $(SH) ext/gen_headers proto.c > proto.gen.h
clean:; rm -f cli srv nil lib9.so
shim: proto; gcc -g -fPIC -shared lib9shim.c -ldl -o lib9.so
.PHONY: clean
