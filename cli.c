// This is free and unencumbered software released into the public domain.
// For more information, see LICENSE.

#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/limits.h>
#include <netdb.h>
#include <errno.h> 
#include <err.h> 
#include "proto.h"
#include "proto.gen.h"

#define USR "anon"

int
tst_chopen(int fd) {
    static char buf[8096];
    size_t size = sizeof(buf);
    uint16_t tag = 0;
    struct Packet *tp = mkchdir(buf, size, tag++, "."), *rp = tp;
    struct PkRInt *ri;
    int res;

    if (send_con(fd, tp) == -1) {
        return -1;
    }
    if ((rp = recv_con(fd, buf, sizeof(buf))) == NULL
        || pck(tp->tag, rp, size) == -1
    ) {
        return -1;
    }
    switch (rp->type) {
    case RError:
        return -1;
    case RInt:
        ri = (struct PkRInt *)rp->data;
        errno = ri->err;
        if ((res = ri->res) == -1) {
            warn("chdir");
        }
    }
    tp = mkopen(buf, size, tag++, 0, 0, "cli.c");
    if (send_con(fd, tp) == -1) {
        return -1;
    }
    if ((rp = recv_con(fd, buf, sizeof(buf))) == NULL
        || pck(tp->tag, rp, size) == -1
    ) {
        return -1;
    }
    switch (rp->type) {
    case RError:
        return -1;
    case RInt:
        ri = (struct PkRInt *)rp->data;
        errno = ri->err;
        if ((res = ri->res) == -1) {
            warn("open");
        }
    }
    return res;
}

int
main(void) {
    int fd;
    char buf[8096];

    if ((fd = clisession()) == -1) {
        err(1, "cannot open session");
    }
    if (clipreamble(fd, USR, buf, sizeof(buf)) == -1) {
        shutdown(fd, SHUT_RDWR);
        return -1;
    }
    tst_chopen(fd);
    return 0;
}
