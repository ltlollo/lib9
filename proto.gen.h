// This is free and unencumbered software released into the public domain.
// For more information, see LICENSE.


struct Packet *
mkver(void *buf, uint32_t size, uint32_t msize, uint16_t tag, const char *ver
    , int type
    );

struct Packet *
mkopen(void *buf, uint32_t size, int16_t tag, int32_t flags, int32_t mode
    , const char *path
    );

struct Packet *
mkchdir(void *buf, uint32_t size, int16_t tag, const char *path);

struct Packet *
mktlogin(void *buf, uint32_t size, uint16_t tag, const char *usr
    , const char *home
    );

int
pck(uint16_t tag, struct Packet *p, uint32_t size);

struct Packet *
mkrlogin(void *buf, uint32_t size, uint16_t tag, const char *home);

struct Packet *
mkerr(void *buf, uint32_t size, uint16_t tag, enum ERR err);

struct Packet *
mktclose(void *buf, uint32_t size, uint16_t tag, int fd);

struct Packet *
mktgetdents64(void *buf, uint32_t size, uint16_t tag, int fd, uint32_t count);

struct Packet *
mkrgetdents64(void *buf, uint32_t size, uint16_t tag, int32_t res, int32_t err
    , void *dirp);

struct Packet *
mkrint(void *buf, uint32_t size, uint16_t tag, int32_t res, int32_t err);

struct Packet *
recv_con(int fd, void *buf, uint32_t size);

int
send_con(int fd, struct Packet *p);

int
ownstr(struct PkStr *s, char **cpy);

int
cpstr(struct PkStr *s, char *cpy, uint32_t size);

int
cliopensock(const char *host, const char *port);

int
clisession(void);

int
clipreamble(int fd, const char *usr, char *buf, uint32_t size);
