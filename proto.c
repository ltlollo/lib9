// This is free and unencumbered software released into the public domain.
// For more information, see LICENSE.

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdint.h>
#include <limits.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/limits.h>
#include <netdb.h>

#include "proto.h"
#include "call.h"

#define min(a, b) ((a) < (b) ? (a) : (b))
#define max(a, b) ((a) > (b) ? (a) : (b))

struct Packet *
mkver(void *buf, uint32_t size, uint32_t msize, uint16_t tag, const char *ver
    , int type
    ) {
    struct Packet *p = (struct Packet *)buf;
    struct PkRVersion *r = (struct PkRVersion *)p->data;
    size_t vlen = strlen(ver);
    size_t req_size = sizeof(struct Packet) + sizeof(struct PkRVersion) + vlen;

    p->size = req_size;
    p->type = type == TRANSMIT ? TVersion : RVersion;
    p->tag = tag;

    if (size < req_size) {
        return NULL;
    }
    r->msize = msize;
    r->vlen = vlen;
    memcpy(r->vstr, ver, r->vlen);
    return p;
}

struct Packet *
mkopen(void *buf, uint32_t size, int16_t tag, int32_t flags, int32_t mode
    , const char *path
    ) {
    struct Packet *p = (struct Packet *)buf;
    struct PkTOpen *r = (struct PkTOpen *)p->data;
    size_t plen = strlen(path);
    size_t req_size = sizeof(struct Packet) + sizeof(struct PkTOpen) + plen;

    p->size = req_size;
    p->type = TOpen;
    p->tag = tag;

    if (size < req_size) {
        return NULL;
    }
    r->flags = flags;
    r->mode = mode;
    r->plen = plen;
    memcpy(r->pstr, path, r->plen);
    return p;
}

struct Packet *
mkchdir(void *buf, uint32_t size, int16_t tag, const char *path) {
    struct Packet *p = (struct Packet *)buf;
    struct PkTChdir *r = (struct PkTChdir *)p->data;
    size_t plen = strlen(path);
    size_t req_size = sizeof(struct Packet) + sizeof(struct PkTChdir) + plen;

    p->size = req_size;
    p->type = TChdir;
    p->tag = tag;

    if (size < req_size) {
        return NULL;
    }
    r->plen = plen;
    memcpy(r->pstr, path, r->plen);
    return p;
}

struct Packet *
mktlogin(void *buf, uint32_t size, uint16_t tag, const char *usr
    , const char *home
    ) {
    struct Packet *p = (struct Packet *)buf;
    size_t ulen = strlen(usr);
    size_t hlen = strlen(home);
    size_t req_size = sizeof(struct Packet) + sizeof(struct PkStr) * 2 + ulen
        + hlen;
    struct PkStr *ustr = (struct PkStr *)(p->data);
    struct PkStr *hstr = (struct PkStr *)(
        p->data + sizeof(struct  PkStr) + ulen
    );

    p->size = req_size;
    p->type = TLogin;
    p->tag = tag;

    if (size < req_size) {
        return NULL;
    }
    ustr->len = ulen;
    memcpy(ustr->str, usr, ustr->len);
    hstr->len = hlen;
    memcpy(hstr->str, home, hstr->len);

    return p;
}

int
pck(uint16_t tag, struct Packet *p, uint32_t size) {
    char *beg = (char *)p, *end = beg + p->size;
    struct PkRError *pe = (struct PkRError *)p->data;
    struct PkTVersion *pv = (struct PkTVersion *)p->data;
    struct PkTChdir *pc = (struct PkTChdir *)p->data;
    struct PkTOpen *po = (struct PkTOpen *)p->data;
    struct PkTGetdents64 *pgd = (struct PkTGetdents64 *)p->data;
    struct PkStr *ustr;
    struct PkStr *hstr;

    if (p->type >= MAX_TYPE || p->tag != tag) {
        return -1;
    }
    switch (p->type) {
        case TError:
            if (p->size != sizeof(struct Packet) + sizeof(struct PkRError)) {
                return -1;
            }
            if (pe->type >= MAX_ERROR) {
                return -1;
            }
            break;
        case RError:
            if (p->size != sizeof(struct Packet) + sizeof(struct PkRError)) {
                return -1;
            }
            if (pe->type >= MAX_ERROR) {
                return -1;
            }
            break;
        case TVersion:
            if (p->size != sizeof(struct Packet) + sizeof(struct PkTVersion)
                + pv->vlen) {
                return -1;
            }
            break;
        case TOpen:
            if (p->size != sizeof(struct Packet) + sizeof(struct PkTOpen)
                + po->plen || po->plen < 1) {
                return -1;
            }
            break;
        case TChdir:
            if (p->size != sizeof(struct Packet) + sizeof(struct PkTChdir)
                + pc->plen || pc->plen < 1) {
                return -1;
            }
            break;
        case RVersion:
            if (p->size != sizeof(struct Packet) + sizeof(struct PkTVersion)
                + pv->vlen) {
                return -1;
            }
            break;
        case TLogin:
            ustr = (struct PkStr *)p->data;
            if (p->data + sizeof(struct PkStr) > end) {
                return -1;
            }
            hstr = (struct PkStr *)(
                p->data + sizeof(struct PkStr) + ustr->len
            );
            if (p->data + 2 * sizeof(struct PkStr) + ustr->len > end) {
                return -1;
            }
            if (p->data + 2 * sizeof(struct PkStr) + ustr->len + hstr->len
                != end
            ) {
                return -1;
            }
            if (ustr->len < 1 || hstr->len < 1) {
                return -1;
            }
            break;
        case RLogin:
            hstr = (struct PkStr *)p->data;
            if (p->data + sizeof(struct PkStr) > end) {
                return -1;
            }
            if (p->data + sizeof(struct PkStr) + hstr->len != end) {
                return -1;
            }
            break;
        case TClose:
        case RInt:
            break;
        case TGetdents64:
        case RGetdents64:
            if (pgd->count >= size - sizeof(struct Packet)
                - sizeof(struct PkTGetdents64)
            ) {
                return -1;
            }
            break;
    }
    return 0;
}

struct Packet *
mkrlogin(void *buf, uint32_t size, uint16_t tag, const char *home) {
    struct Packet *p = (struct Packet *)buf;
    size_t hlen = strlen(home);
    size_t req_size = sizeof(struct Packet) + sizeof(struct PkStr) + hlen;
    struct PkStr *hstr = (struct PkStr *)(p->data);

    p->size = req_size;
    p->type = RLogin;
    p->tag = tag;

    if (size < req_size) {
        return NULL;
    }
    hstr->len = hlen;
    memcpy(hstr->str, home, hstr->len);

    return p;
}

struct Packet *
mkerr(void *buf, uint32_t size, uint16_t tag, enum ERR err) {
    struct Packet *p = (struct Packet *)buf;
    struct PkRError *r = (struct PkRError *)p->data;
    uint32_t req_size = sizeof(struct Packet) + sizeof(struct PkRError);

    p->size = req_size;
    p->type = RError;
    p->tag = tag;

    if (size < req_size) {
        return NULL;
    }
    r->type = err;
    return p;
}

struct Packet *
mktclose(void *buf, uint32_t size, uint16_t tag, int fd) {
    struct Packet *p = (struct Packet *)buf;
    struct PkTClose *r = (struct PkTClose *)p->data;
    uint32_t req_size = sizeof(struct Packet) + sizeof(struct PkTClose);

    p->size = req_size;
    p->type = TClose;
    p->tag = tag;

    if (size < req_size) {
        return NULL;
    }
    r->fd = fd;
    return p;
}

struct Packet *
mktgetdents64(void *buf, uint32_t size, uint16_t tag, int fd, uint32_t count) {
    struct Packet *p = (struct Packet *)buf;
    struct PkTGetdents64 *t = (struct PkTGetdents64 *)p->data;
    size_t req_size = sizeof(struct Packet) + sizeof(struct PkTGetdents64);

    p->size = req_size;
    p->type = TGetdents64;
    p->tag = tag;

    if (size < req_size) {
        return NULL;
    }
    count = min(size - sizeof(struct PkRGetdents64) - sizeof(struct Packet)
        , count
    );
    t->fd = fd;
    t->count = count;
    return p;
}

struct Packet *
mkrgetdents64(void *buf, uint32_t size, uint16_t tag, int32_t res, int32_t err
    , void *dirp) {
    struct Packet *p = (struct Packet *)buf;
    struct PkRGetdents64 *r = (struct PkRGetdents64 *)p->data;
    size_t req_size = sizeof(struct Packet) + sizeof(struct PkRGetdents64);

    if (res < -1) {
        return NULL;
    } else if (res != -1) {
        req_size += res;
    }
    p->size = req_size;
    p->type = RGetdents64;
    p->tag = tag;

    if (size < req_size) {
        return NULL;
    }
    r->res = res;
    r->err = err;
    if (res != -1) {
        memmove(r->dirp, dirp, res);
    }
    return p;
}

struct Packet *
mkrint(void *buf, uint32_t size, uint16_t tag, int32_t res, int32_t err) {
    struct Packet *p = (struct Packet *)buf;
    struct PkRInt *r = (struct PkRInt *)p->data;
    uint32_t req_size = sizeof(struct Packet) + sizeof(struct PkRInt);

    p->size = req_size;
    p->type = RInt;
    p->tag = tag;

    if (size < req_size) {
        return NULL;
    }
    r->res = res;
    r->err = err;
    return p;
}

struct Packet *
recv_con(int fd, void *buf, uint32_t size) {
    struct Packet *com = (struct Packet *)buf;
    ssize_t res, rst = sizeof(struct Packet);

HEADER:
    while (rst && (res = recv(fd, buf, rst, 0)) > 0) {
        rst -= res;
    }
    if (res == 0) {
        errno = ECONNABORTED;
        return NULL;
    } else if (res == -1) {
        switch (errno) {
            case EAGAIN: goto HEADER;
            default: return NULL;
        }
    }
    if (com->size < sizeof(struct Packet) || com->size >= size) {
        return NULL;
    }
BODY:
    rst = com->size - sizeof(struct Packet);
    while (rst && (res = recv(fd, com->data, rst, 0)) > 0) {
        rst -= res;
    }
    if (res == 0) {
        return NULL;
    } else if (res == -1) {
        switch (errno) {
            case EAGAIN: goto BODY;
            default: return NULL;
        }
    }
    return com;
}

int
send_con(int fd, struct Packet *p) {
    ssize_t rst = p->size, snd;
SEND:
    while (rst && (snd = send(fd, p, rst, 0)) > 0) {
        rst -= snd;
    }
    if (snd == 0) {
        errno = ECONNABORTED;
        return -1;
    } else if (snd == -1) {
        switch (errno) {
            case EAGAIN: goto SEND;
            default: return -1;
        }
    }
    return 0;
}

int
ownstr(struct PkStr *s, char **cpy) {
    void *tmp;
    
    if (s->len == USHRT_MAX || (tmp = realloc(*cpy, s->len + 1)) == NULL) {
        return -1;
    }
    memcpy(*cpy = tmp, s->str, s->len);
    (*cpy)[s->len] = '\0';
    
    return 0;
}

int
cpstr(struct PkStr *s, char *cpy, uint32_t size) {
    if (s->len == USHRT_MAX || size < s->len + 1u) {
        return -1;
    }
    memcpy(cpy, s->str, s->len);
    cpy[s->len] = '\0';

    return 0;
}

int
cliopensock(const char *host, const char *port) {
    struct addrinfo hints = {
        .ai_family = AF_UNSPEC,
        .ai_socktype = SOCK_STREAM,
    }, *res, *rp;
    int fd;

    if (getaddrinfo(host, port, &hints, &res) != 0) {
        return -1;
    }
    for (rp = res; rp; rp = rp->ai_next) {
        fd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (fd == -1) {
            continue;
        }
        if (connect(fd, rp->ai_addr, rp->ai_addrlen) == -1) {
            close(fd);
            continue;
        }
        break;
    }
    freeaddrinfo(res);
    return rp == NULL ? -1 : fd;
}

int
clisession(void) {
    int fd;
    char *host = getenv("HOST9");
    char *port = NULL;

    if (host == NULL) {
        host = "127.0.0.1";
    }
    host = strdup(host);
    if ((port = strchr(host, ':')) == NULL) {
        port = getenv("PORT9");
    } else  {
        *port++ = '\0';
    }
    if (port == NULL) {
        port = "12345";
    }
    fd = cliopensock(host, port);
    free(host);

    return fd;
}

int
clipreamble(int fd, const char *usr, char *buf, uint32_t size) {
    char *str = malloc(max(size + 1, PATH_MAX));
    struct Packet *tp = mkver(buf, size, size, ~0, VER, TRANSMIT), *rp = tp;
    uint16_t tag = 0;
    struct PkRVersion *rvp = (void *)rp->data;
    struct PkStr *pwd;
    int err = -1;

    if (str == NULL) {
        goto FINPREABLE;
    }
    if (send_con(fd, tp) == -1) {
        goto FINPREABLE;
    }
    if ((rp = recv_con(fd, buf, size)) == NULL
        || pck(tp->tag, rp, size) == -1
    ) {
        goto FINPREABLE;
    }
    switch (rp->type) {
    case RError:
        goto FINPREABLE;
    case RVersion:
        size = min(rvp->msize, size);
        if (memcmp(VER, rvp->vstr, rvp->vlen) != 0) {
            goto FINPREABLE;
        }
        break;
    }
    if (realpath(".", str) == NULL) {
        goto FINPREABLE;
    }
    if ((tp = mktlogin(buf, size, tag++, usr, str)) == NULL) {
        goto FINPREABLE;
    }
    if (send_con(fd, tp) == -1) {
        goto FINPREABLE;
    }
    if ((rp = recv_con(fd, buf, size)) == NULL
        || pck(tp->tag, rp, size) == -1
    ) {
        goto FINPREABLE;
    }
    switch (rp->type) {
        default:
            goto FINPREABLE;
        case RLogin:
            pwd = (struct PkStr *)rp->data;
            cpstr(pwd, str, sizeof(str));
            break;
    }
    err = 0;
FINPREABLE:
    free(str);
    return err;
}
